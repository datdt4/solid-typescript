import {Database} from "../../single/responsibility/cohension/ex3/Database";

export class Post {

    CreatePost(db: Database, postMessage: string) {
        if (postMessage.startsWith("#")) {
            db.addAsTag(postMessage);
        }

        if (postMessage.startsWith("mention")) {
            db.addAsMentionPost(postMessage);
        }
        else {
            db.Add(postMessage);
        }

    }
}

