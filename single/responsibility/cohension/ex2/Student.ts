
export class Student {

    id: number;

    name: String;

    age: number;

    public getId(): number {
        return this.id;
    }

    public setId(id: number) {
        this.id = id;
    }

    public getName(): String {
        return this.name;
    }

    public setName(name: String) {
        this.name = name;
    }

    public getAge(): number {
        return this.age;
    }

    public setAge(age: number) {
        this.age = age;
    }

    public saveToDatabase(): boolean {
        let url: String = "jdbc:msql://xxx.yyyy.zzz.lll:3306/Demo";
        let conn: Connection = DriverManager.getConnection(url, "", "");
        let st: Statement = conn.createStatement();
        st.executeUpdate(("INSERT INTO Customers " + "VALUES (1001, 'Simpson', 'Mr.', 'Springfield', 2001)"));
        return true;
    }
}

